package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.Objects;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private String xmlFileName;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE
    private Flowers flowers;
    private String currentElement;
    private Flower flower;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private AveLenFlower aveLenFlower;
    private Tempreture temperature;
    private Lighting lighting;
    private Watering watering;

    public String getXmlFileName() {
        return xmlFileName;
    }

    public Flowers getFlowers() {
        return flowers;
    }

    public String getCurrentElement() {
        return currentElement;
    }

    public Flower getFlower() {
        return flower;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public AveLenFlower getAveLenFlower() {
        return aveLenFlower;
    }

    public Tempreture getTemperature() {
        return temperature;
    }

    public Lighting getLighting() {
        return lighting;
    }

    public Watering getWatering() {
        return watering;
    }

    public void parse(boolean validate) throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance(
                "com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl",
                this.getClass().getClassLoader());

        factory.setNamespaceAware(true);
        if (validate) {
            factory.setFeature("http://xml.org/sax/features/validation", true);
            factory.setFeature("http://apache.org/xml/features/validation/schema", true);
        }
        SAXParser parser = factory.newSAXParser();
        parser.parse(xmlFileName, this);
    }

    @Override
    public void error(org.xml.sax.SAXParseException e) throws SAXException {
        throw e;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        currentElement = localName;
        if (Objects.equals(currentElement, "flowers")) {
            flowers = new Flowers();
            return;
        }
        if (Objects.equals(currentElement, "flower")) {
            flower = new Flower();
            flowers.getFlowers().add(flower);
            return;
        }
        if (Objects.equals(currentElement, "visualParameters")) {
            visualParameters = new VisualParameters();
            flower.setVisualParameters(visualParameters);
            return;
        }
        if (Objects.equals(currentElement, "growingTips")) {
            growingTips = new GrowingTips();
            flower.setGrowingTips(growingTips);
            return;
        }
        if (Objects.equals(currentElement, "aveLenFlower")) {
            aveLenFlower = new AveLenFlower();
            if (attributes.getLength() > 0) {
                aveLenFlower.setMeasure(attributes.getValue(uri, "measure"));
            }
            flower.getVisualParameters().setAveLenFlower(aveLenFlower);
        }
        if (Objects.equals(currentElement, "tempreture")) {
            temperature = new Tempreture();
            if (attributes.getLength() > 0) {
                temperature.setMeasure(attributes.getValue(uri, "measure"));
            }
            flower.getGrowingTips().setTempreture(temperature);
        }
        if (Objects.equals(currentElement, "lighting")) {
            lighting = new Lighting();
            if (attributes.getLength() > 0) {
                lighting.setLightRequiring(attributes.getValue("lightRequiring"));
            }
            flower.getGrowingTips().setLighting(lighting);
        }
        if (Objects.equals(currentElement, "watering")) {
            watering = new Watering();
            if (attributes.getLength() > 0) {
                watering.setMeasure(attributes.getValue(uri, "measure"));
            }
            flower.getGrowingTips().setWatering(watering);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String elementText = new String(ch, start, length);
        if (elementText.isEmpty()) {
            return;
        }
        if (Objects.equals(currentElement, "name")) {
            flower.setName(elementText);
            return;
        }
        if (Objects.equals(currentElement, "soil")) {
            flower.setSoil(elementText);
            return;
        }
        if (Objects.equals(currentElement, "origin")) {
            flower.setOrigin(elementText);
            return;
        }
        if (Objects.equals(currentElement, "multiplying")) {
            flower.setMultiplying(elementText);
            return;
        }
        if (Objects.equals(currentElement, "stemColour")) {
            visualParameters.setStemColour(elementText);
            return;
        }
        if (Objects.equals(currentElement, "leafColour")) {
            visualParameters.setLeafColour(elementText);
            return;
        }
        if (Objects.equals(currentElement, "aveLenFlower")) {
            aveLenFlower.setValue(Integer.parseInt(elementText));
            return;
        }
        if (Objects.equals(currentElement, "tempreture")) {
            temperature.setValue(Integer.parseInt(elementText));
            return;
        }
        if (Objects.equals(currentElement, "watering")) {
            watering.setValue(Integer.parseInt(elementText));
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (Objects.equals(currentElement, "flower")) {
            flowers.getFlowers().add(flower);
            return;
        }
        if (Objects.equals(currentElement, "visualParameters")) {
            flower.setVisualParameters(visualParameters);
            return;
        }
        if (Objects.equals(currentElement, "growingTips")) {
            flower.setGrowingTips(growingTips);
        }
    }
}