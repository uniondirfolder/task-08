package com.epam.rd.java.basic.task8;

import java.util.Comparator;

public class Sorter {
    private Sorter(){}

    public static void sortFlowersByName(Flowers flowers) {
        flowers.getFlowers().sort(
                Comparator.comparing(Flower::getName)
        );
    }

    public static void sortFlowersByAveLen(Flowers flowers) {
        flowers.getFlowers().sort((v1, v2) ->
                v2.getVisualParameters()
                        .getAveLenFlower()
                        .getValue().compareTo(v1.getVisualParameters().getAveLenFlower().getValue())
        );
    }

    public static void sortFlowersByWatering(Flowers flowers) {
        flowers.getFlowers().sort((v1, v2) ->
                v2.getGrowingTips()
                        .getWatering()
                        .getValue()
                        .compareTo(v1.getGrowingTips().getWatering().getValue())
        );
    }
}
