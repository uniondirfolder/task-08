package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;


/**
 * Controller for DOM parser.
 */
public class DOMController {

    private String xmlFileName;
    private Flowers flowers;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE
    public Flowers getFlowers() {
        return flowers;
    }

    public String getXmlFileName() {
        return xmlFileName;
    }

    public void parse(boolean validate) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(
                "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl",
                this.getClass().getClassLoader()
        );

        dbf.setNamespaceAware(true);

        if (validate) {
            dbf.setFeature("http://xml.org/sax/features/validation", true);
            dbf.setFeature("http://apache.org/xml/features/validation/schema", true);
        }

        DocumentBuilder db = dbf.newDocumentBuilder();

        db.setErrorHandler(new DefaultHandler() {
            @Override
            public void error(SAXParseException e) throws SAXException {
                throw e;
            }
        });

        Document document = db.parse(xmlFileName);

        Element root = document.getDocumentElement();

        flowers = new Flowers();

        NodeList flowersNodes = root.getElementsByTagName("flower");

        for (int i = 0; i < flowersNodes.getLength(); i++) {
            Flower flower = getFlower(flowersNodes.item(i));
            flowers.getFlowers().add(flower);
        }
    }

    private Flower getFlower(Node item) {
        Flower flower = new Flower();
        Element flowerElement = (Element) item;

        flower.setName(
                flowerElement.getElementsByTagName("name").item(0).getTextContent()
        );
        flower.setSoil(
                flowerElement.getElementsByTagName("soil").item(0).getTextContent()
        );
        flower.setOrigin(
                flowerElement.getElementsByTagName("origin").item(0).getTextContent()
        );
        flower.setMultiplying(
                flowerElement.getElementsByTagName("multiplying").item(0).getTextContent()
        );
        flower.setVisualParameters(
                getVisualParameters(flowerElement.getElementsByTagName("visualParameters").item(0))
        );
        flower.setGrowingTips(
                getGrowingTips(flowerElement.getElementsByTagName("growingTips").item(0))
        );
        return flower;
    }

    private GrowingTips getGrowingTips(Node growingTips) {
        GrowingTips tips = new GrowingTips();
        Element growingElement = (Element) growingTips;

        Tempreture temperature = new Tempreture();
        Lighting light = new Lighting();
        Watering watering = new Watering();

        Element temperatureElement = (Element) growingElement.getElementsByTagName("tempreture").item(0);
        Element lightElement = (Element) growingElement.getElementsByTagName("lighting").item(0);
        Element wateringElement = (Element) growingElement.getElementsByTagName("watering").item(0);

        temperature.setValue(
                Integer.parseInt(
                        temperatureElement.getTextContent()
                )
        );
        temperature.setMeasure(temperatureElement.getAttribute("measure"));
        light.setLightRequiring(lightElement.getAttribute("lightRequiring"));
        watering.setValue(
                Integer.parseInt(
                        wateringElement.getTextContent()
                )
        );
        watering.setMeasure(wateringElement.getAttribute("measure"));

        tips.setTempreture(temperature);
        tips.setLighting(light);
        tips.setWatering(watering);

        return tips;
    }

    private VisualParameters getVisualParameters(Node visualParameters) {
        Element visualParametersElement = (Element)visualParameters;
        Element aveLenFlowerElement = (Element)visualParametersElement.getElementsByTagName("aveLenFlower").item(0);

        VisualParameters parameters = new VisualParameters();
        AveLenFlower aveLenFlower = new AveLenFlower();

        aveLenFlower.setValue(
                Integer.parseInt(aveLenFlowerElement.getTextContent())
        );

        aveLenFlower.setMeasure(
                aveLenFlowerElement.getAttribute("measure")
        );

        parameters.setAveLenFlower(aveLenFlower);

        parameters.setStemColour(
                visualParametersElement.getElementsByTagName("stemColour").item(0).getTextContent()
        );

        parameters.setLeafColour(
                visualParametersElement.getElementsByTagName("leafColour").item(0).getTextContent()
        );

        return parameters;
    }

    public static void saveXML(Flowers flowers, String xmlFileName) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(
                "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl",
                DOMController.class.getClassLoader());

        dbf.setNamespaceAware(true);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.newDocument();

        Element rootElement = document.createElement("flowers");

        rootElement.setAttribute("xmlns", "http://www.nure.ua");
        rootElement.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");

        document.appendChild(rootElement);

        for (Flower flower : flowers.getFlowers()) {
            Element flowerElement = document.createElement("flower");
            rootElement.appendChild(flowerElement);

            Element nameElement = document.createElement("name");
            nameElement.setTextContent(flower.getName());
            flowerElement.appendChild(nameElement);

            Element soilElement = document.createElement("soil");
            soilElement.setTextContent(flower.getSoil());
            flowerElement.appendChild(soilElement);

            Element originElement = document.createElement("origin");
            originElement.setTextContent(flower.getOrigin());
            flowerElement.appendChild(originElement);

            Element visualParametersElement = document.createElement("visualParameters");

            Element stemColourElement = document.createElement("stemColour");
            stemColourElement.setTextContent(flower.getVisualParameters().getStemColour());

            Element leafColourElement = document.createElement("leafColour");
            leafColourElement.setTextContent(flower.getVisualParameters().getLeafColour());

            Element aveLenFlowerElement = document.createElement("aveLenFlower");
            aveLenFlowerElement.setTextContent(flower.getVisualParameters().getAveLenFlower().getValue().toString());
            aveLenFlowerElement.setAttribute("measure", flower.getVisualParameters().getAveLenFlower().getMeasure());

            visualParametersElement.appendChild(stemColourElement);
            visualParametersElement.appendChild(leafColourElement);
            visualParametersElement.appendChild(aveLenFlowerElement);

            flowerElement.appendChild(visualParametersElement);

            Element growingTipsElement = document.createElement("growingTips");

            Element temperatureElement = document.createElement("tempreture");
            temperatureElement.setTextContent(flower.getGrowingTips().getTempreture().getValue().toString());
            temperatureElement.setAttribute("measure", flower.getGrowingTips().getTempreture().getMeasure());

            Element lightingElement = document.createElement("lighting");
            lightingElement.setAttribute("lightRequiring", flower.getGrowingTips().getLighting().getLightRequiring());

            Element wateringElement = document.createElement("watering");
            wateringElement.setTextContent(flower.getGrowingTips().getWatering().getValue().toString());
            wateringElement.setAttribute("measure", flower.getGrowingTips().getWatering().getMeasure());

            growingTipsElement.appendChild(temperatureElement);
            growingTipsElement.appendChild(lightingElement);
            growingTipsElement.appendChild(wateringElement);

            flowerElement.appendChild(growingTipsElement);

            Element multiplyingElement = document.createElement("multiplying");
            multiplyingElement.setTextContent(flower.getMultiplying());
            flowerElement.appendChild(multiplyingElement);
        }

        saveDOMToXML(document, xmlFileName);
    }

    private static void saveDOMToXML(Document document, String xmlFileName) throws TransformerException {
        StreamResult result = new StreamResult(new File(xmlFileName));
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty("indent", "yes");
        transformer.transform(new DOMSource(document), result);
    }

}
