package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			System.out.println("Mismatch in parameters");
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		domController.parse(true);
		Flowers flowersDom = domController.getFlowers();
		// sort (case 1)
		// PLACE YOUR CODE HERE
		Sorter.sortFlowersByName(flowersDom);
		// save

		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		DOMController.saveXML(flowersDom, outputXmlFile);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		saxController.parse(true);
		Flowers flowersSax = saxController.getFlowers();
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		Sorter.sortFlowersByAveLen(flowersSax);
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		DOMController.saveXML(flowersSax, outputXmlFile);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		staxController.parse();
		Flowers flowersStax = staxController.getFlowers();
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		Sorter.sortFlowersByWatering(flowersStax);
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		DOMController.saveXML(flowersStax, outputXmlFile);

	}

}
